# Starjet
[![Generic badge](https://img.shields.io/badge/Using-Docker-blue.svg)](https://shields.io/) [![Generic badge](https://img.shields.io/badge/Using-Kubernetes-blue.svg)](https://shields.io/) [![Generic badge](https://img.shields.io/badge/Using-DockerSwarm-blue.svg)](https://shields.io/) [![made-with-python](https://img.shields.io/badge/Made%20with-Python-1f425f.svg)](https://www.python.org/) [![Open Source? Yes!](https://badgen.net/badge/Open%20Source%20%3F/Yes%21/blue?icon=github)](https://github.com/Naereen/badges/) [![Maintenance](https://img.shields.io/badge/Maintained%3F-Yes-green.svg)](https://gitlab.com/WarpWing/StarJet/-/commits/master)


StarJet is the name of my DevOps Playground. You will find everything from Kubernetes to Docker to Node.JS and beyond! 

Please look around and write a PR if you would like to contribute!

## License
[MIT](https://choosealicense.com/licenses/mit/)
